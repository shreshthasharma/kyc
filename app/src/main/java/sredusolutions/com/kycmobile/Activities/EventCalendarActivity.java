package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

public class EventCalendarActivity extends AppCompatActivity {

    private static final String TAG = EventCalendarActivity.class.getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.compactcalendar_view)
    CompactCalendarView mCompactCalendarView;
    @Bind(R.id.calendar_heading)
    TextView mCalendarHeading;

    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat ddmmyyDateFormat = new SimpleDateFormat("y-M-dd",Locale.getDefault());
    private ProgressDialog pd;
    private MySharedPreferences sp;
    private RequestQueue mRequestQueue;

    private List<String> mEventStringList;
    private List<Event> mEventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_calendar);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Event");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        mCompactCalendarView.setUseThreeLetterAbbreviation(true);

        mEventStringList = new ArrayList<>();
        mEventList = new ArrayList<>();

        //get current time
        mCalendarHeading.setText(dateFormatMonth.format(new Date(System.currentTimeMillis())));

        mCompactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                String[] date = ddmmyyDateFormat.format(dateClicked).split("-");
                Intent intent = new Intent(EventCalendarActivity.this,EventsActivity.class);
                intent.putExtra("year",date[0]);
                intent.putExtra("month",date[1]);
                intent.putExtra("day",date[2]);
                startActivity(intent);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                mCalendarHeading.setText(dateFormatMonth.format(firstDayOfNewMonth));
            }
        });

        sp = MySharedPreferences.getInstance(this);

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));

        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        checkInternetAndFetch();
    }

    /*
    * Method to fetch event in backgroud
    * */
    private void checkInternetAndFetch() {
        if (Utils.isNetworkAvailable()){
            fetchEventInBackground();
        }else{
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    /*
    * Method to fetch event Data in background
    * */
    private void fetchEventInBackground() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.EVENTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG,"HUS: "+error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("mobile",sp.getKey(MySharedPreferences.KEY_MOBILE));
                map.put("api",sp.getKey(MySharedPreferences.KEY_APIKEY));
                return map;
            }
        };

        mRequestQueue.add(request);
    }

    /*
    * Parse Response
    * */
    private void parseResponse(String response) {
        try {
            JSONObject jo = new JSONObject(response);
            if (jo.getString("error") != null){
                //success
                JSONArray jsonArray = jo.getJSONArray("data");
                for(int i = 0;i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String dateStart = jsonObject.getString("dateStart");
                    //add date to eventStringList if its not edited
                    if (!mEventStringList.contains(dateStart)){
                        mEventStringList.add(dateStart);
                    }
                }

                initEventsOnCalendar();
            }else{
                //error
                Toast.makeText(getApplicationContext(),jo.getString("message"),Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showParsingErrorAlert(EventCalendarActivity.this);
        }
    }

    //Method to add events on calendar
    private void initEventsOnCalendar() {
        Calendar ca = Calendar.getInstance();
        for (int i = 0; i < mEventStringList.size(); i++) {
            String event = mEventStringList.get(i);
            String[] dates = event.split("-");
            int year = Integer.parseInt(dates[0]);
            int month = Integer.parseInt(dates[1]) - 1;
            int day = Integer.parseInt(dates[2]);
            ca.set(year,month,day);

            //add event
            Event ev = new Event(R.color.accent,ca.getTimeInMillis());
            mEventList.add(ev);
        }

        //add events to calendar
        mCompactCalendarView.addEvents(mEventList);
    }
}
