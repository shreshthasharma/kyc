package sredusolutions.com.kycmobile.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.R;

public class EventDetailActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    @Bind(R.id.heading) TextView mHeading;
    @Bind(R.id.description) TextView mDescription;
    @Bind(R.id.startDate) TextView mStartDate;
    @Bind(R.id.endDate) TextView mEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);


        Intent intent = getIntent();
        String endDate = intent.getStringExtra("endDate");
        String startDate = intent.getStringExtra("starDate");
        String detail = intent.getStringExtra("detail");
        String shortName = intent.getStringExtra("shortName");

        mHeading.setText(shortName);
        mDescription.setText(detail);
        mStartDate.setText(startDate);
        mEndDate.setText(endDate);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle(shortName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }
}
