package sredusolutions.com.kycmobile.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sredusolutions.com.kycmobile.Adapter.EventAdapter;
import sredusolutions.com.kycmobile.Adapter.HwcwAdapter;
import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.Pojo.EventPojo;
import sredusolutions.com.kycmobile.Pojo.HwcwPojo;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

public class EventsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = EventsActivity.class.getSimpleName();
    RecyclerView rv;

    private Calendar calendar;
    private int year, month, day;
    TextView date;
    ImageView dropdown;
    private ProgressDialog pd;
    private MySharedPreferences sp;
    private RequestQueue mRequestQueue;

    public List<EventPojo> eventList;
    private String dateString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Event");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        //get date from intent
        Intent intent = getIntent();
        String yearString = intent.getExtras().getString("year");
        String monthString = intent.getExtras().getString("month");
        String dayString = intent.getExtras().getString("day");

        if (Integer.parseInt(monthString) < 10){
            monthString = String.valueOf(0 + monthString);
        }

        dateString = yearString+"-"+monthString+"-"+dayString;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        eventList = new ArrayList<>();

        //Set adapter for show data
        rv = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        date = (TextView)findViewById(R.id.date);
        date.setOnClickListener(this);

        dropdown = (ImageView)findViewById(R.id.down);
        dropdown.setOnClickListener(this);

        sp = MySharedPreferences.getInstance(this);

        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));

        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        checkInternetAndFetch();
    }

    /*
    * Method to fetch event in backgroud
    * */
    private void checkInternetAndFetch() {
        if (Utils.isNetworkAvailable()){
            fetchEventInBackground();
        }else{
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    /*
    * Method to fetch event Data in background
    * */
    private void fetchEventInBackground() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.EVENTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG,"HUS: "+error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("mobile",sp.getKey(MySharedPreferences.KEY_MOBILE));
                map.put("api",sp.getKey(MySharedPreferences.KEY_APIKEY));
                return map;
            }
        };

        mRequestQueue.add(request);
    }

    /*
    * Parse Response
    * */
    private void parseResponse(String response) {
        try {
            JSONObject jo = new JSONObject(response);
            if (jo.getString("error") != null){
                //success
                JSONArray jsonArray = jo.getJSONArray("data");
                for(int i = 0;i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String id = jsonObject.getString("id");
                    String dateEnd = jsonObject.getString("dateEnd");
                    String dateStart = jsonObject.getString("dateStart");
                    String detail = jsonObject.getString("details");
                    String division = jsonObject.getString("division");
                    String schoolId = jsonObject.getString("schoolId");
                    String shortName = jsonObject.getString("shortName");
                    String stdId = jsonObject.getString("stdId");
                    String active = jsonObject.getString("active");

                    if (dateStart.equals(dateString)){
                        EventPojo eventPojo = new EventPojo();

                        eventPojo.setId(id);
                        eventPojo.setEndDate(dateEnd);
                        eventPojo.setStartDate(dateStart);
                        eventPojo.setDetails(detail);
                        eventPojo.setDivision(division);
                        eventPojo.setSchoolId(schoolId);
                        eventPojo.setShortName(shortName);
                        eventPojo.setStdId(stdId);
                        eventPojo.setActive(active);

                        eventList.add(eventPojo);
                    }

                }

                setAdapter();
            }else{
                //error
                Toast.makeText(getApplicationContext(),jo.getString("message"),Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showParsingErrorAlert(EventsActivity.this);
        }
    }

    private void setAdapter() {
        EventAdapter adapter = new EventAdapter(eventList,this);
        rv.setAdapter(adapter);
    }

    private void getaDate() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.setText("");
                date.append(dayOfMonth + "/" + (monthOfYear +1) + "/" + year);
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.date:
                getaDate();
                break;
            case R.id.down:
                getaDate();
                break;
        }

    }


}