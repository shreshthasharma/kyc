package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Adapter.SubjectAdapter;
import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.Pojo.SubjectPojo;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;


public class HomeWorkActivity extends AppCompatActivity{
    private static final String TAG = HomeWorkActivity.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ProgressDialog pd;
    private MySharedPreferences sp;
    private String mType;

    private List<SubjectPojo> mList;
    private List<String> mNotEmptySubjectList;
    private List<SubjectPojo> mSubjectList;

    @Bind(R.id.layout) LinearLayout mLayoutForSnackbar;
    @Bind(R.id.recyclerView) RecyclerView mRecyclerView;
    @Bind(R.id.user_id) TextView mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);
        ButterKnife.bind(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        Intent intent = getIntent();
        if (intent.hasExtra("type")){
            mType = intent.getStringExtra("type");
            setSupportActionBar(mToolbar);
            assert getSupportActionBar() != null;
            if (mType.equals("hw")){
                getSupportActionBar().setTitle("Home Work");
            }else{
                getSupportActionBar().setTitle("Class Work");
            }
        }else{
            setSupportActionBar(mToolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle("Home Work");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        //init pd
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));

        //init request queue
        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        //init sp
        sp = MySharedPreferences.getInstance(this);

        //set user id
        mUserId.setText(sp.getKey(MySharedPreferences.KEY_USER_ID));

        //list init
        mList = new ArrayList<>();
        mNotEmptySubjectList = new ArrayList<>();
        mSubjectList = new ArrayList<>();

        //init rc
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        checkInternetAndFetchHWCW();
    }

    private void checkInternetAndFetchHWCW() {
        if (Utils.isNetworkAvailable()){
            fetchHWCWInBG();
        }else{
            Utils.noInternetSnackBar(this, mLayoutForSnackbar);
        }
    }

    /*
    * Fetch HWCW in bg
    * */
    private void fetchHWCWInBG() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.HWCW_ALL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                parseResponseHWCW(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG,"HUS: "+error.getMessage());
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("mobile",sp.getKey(MySharedPreferences.KEY_MOBILE));
                params.put("api",sp.getKey(MySharedPreferences.KEY_APIKEY));
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(request);
    }

    /*
    * Parse HWCW
    * */
    private void parseResponseHWCW(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("error") != null){
                //success
                JSONArray data = jsonObject.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONArray ja = data.getJSONArray(i);
                    String id = ja.getString(0);
                    String subject_id = ja.getString(1);
                    String daily_update_id = ja.getString(2);
                    String class_work = ja.getString(3);
                    String home_work = ja.getString(4);
                    String time = ja.getString(5);

                    //set all the sub
                    if (!mNotEmptySubjectList.contains(subject_id)){
                        mNotEmptySubjectList.add(subject_id);
                    }
                }

                fetchSubjectMaster();
            }else{
                //Error
                Utils.showRedSnackbar(mLayoutForSnackbar,jsonObject.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showParsingErrorAlert(HomeWorkActivity.this);
        }
    }

    /*
    * Method to fetch subject master
    * */
    private void fetchSubjectMaster() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, Endpoints.SUBJECTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                parseSubjectResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG, "HUS: onErrorResponse: "+error.getMessage());
                Toast.makeText(getApplicationContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("api",sp.getKey(MySharedPreferences.KEY_APIKEY));
                params.put("mobile",sp.getKey(MySharedPreferences.KEY_MOBILE));
                return params;
            }
        };
        mRequestQueue.add(request);
    }

    /*
    * Method to parse Subject
    * */
    private void parseSubjectResponse(String response) {
        Log.e(TAG, "HUS: parseSubjectResponse: "+response);
        try {
            JSONObject jo = new JSONObject(response);
            if (jo.getString("error") != null){
                //success
                JSONArray data = jo.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonObject = data.getJSONObject(i);
                    String id = jsonObject.getString("id");
                    String name = jsonObject.getString("name");
                    String shortName = jsonObject.getString("shortName");

                    //only add those subject which have data
                    if (mNotEmptySubjectList.contains(id)){
                        SubjectPojo hw = new SubjectPojo();
                        hw.setId(id);
                        hw.setShortName(shortName);
                        hw.setName(name);
                        mSubjectList.add(hw);
                    }
                }

                setupRc();
            }else{
                //error
                String message = jo.getString("message");
                Utils.showRedSnackbar(mLayoutForSnackbar,message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.showParsingErrorAlert(HomeWorkActivity.this);
        }
    }

    /*
    * Method to setup subject recyclerview
    * */
    private void setupRc() {
        SubjectAdapter adapter = new SubjectAdapter(this,mSubjectList,mType);
        mRecyclerView.setAdapter(adapter);
    }
}
