package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Adapter.HwcwAdapter;
import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.Pojo.HwcwPojo;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

public class HomeWorkCommonActivity extends AppCompatActivity{
    private static final String TAG = HomeWorkCommonActivity.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ProgressDialog pd;
    private RecyclerView rv;

    private View layoutForSnackbar;
    private MySharedPreferences sp;

    public List<HwcwPojo> hwcwList;

    private String mId;
    private String mType;
    @Bind(R.id.user_id) TextView mUserId;
    @Bind(R.id.refresh_btn) ImageView mRefreshBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_work);
        ButterKnife.bind(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        layoutForSnackbar = findViewById(R.id.activity_homework);

        //init sp
        sp = MySharedPreferences.getInstance(this);

        Intent intent = getIntent();
        mType = intent.getStringExtra("type");

        mId = intent.getStringExtra("id");
        String title = intent.getStringExtra("title");

        //set title
        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        //init pd
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));

        //init list
        hwcwList = new ArrayList<>();

        //init request queue
        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        //Set adapter for show data
        rv = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        //set user id
        mUserId.setText(sp.getKey(MySharedPreferences.KEY_USER_ID));

        //get data from cache
        String response = getJsonFromCache();
        if (response != null && !response.isEmpty()){
            //parse data from json
            parseResponse(response);
        }else{
            //download fresh data
            checkInternetConnection();
        }

        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternetConnection();
            }
        });
    }

    private void checkInternetConnection() {
        if (Utils.isNetworkAvailable()) {
            lodingData();
        } else {
            Utils.noInternetSnackBar(this, layoutForSnackbar);
        }
    }

    private void lodingData() {
        pd.show();
        StringRequest sRequest = new StringRequest(Request.Method.POST, Endpoints.HWCW_ALL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                //save json to cache
                saveJsonToCache(response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG, "HUS: lodingData: onErrorResponse: " + error.getMessage());
                Utils.showParsingErrorAlert(HomeWorkCommonActivity.this);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api", sp.getKey(MySharedPreferences.KEY_APIKEY));
                params.put("mobile", sp.getKey(MySharedPreferences.KEY_MOBILE));
                return params;
            }
        };

        sRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(sRequest);
    }

    /*
    * Method to parse response
    * */
    private void parseResponse(String response) {
        try {
            JSONObject jo = new JSONObject(response);

            if (jo.getString("error") != null){

                JSONArray data = jo.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONArray ja = data.getJSONArray(i);
                    String id = ja.getString(0);
                    String subject_id = ja.getString(1);
                    String daily_update_id = ja.getString(2);
                    String class_work = ja.getString(3);
                    String home_work = ja.getString(4);
                    String time = ja.getString(5);

                    if (mId.equals(subject_id)){
                        HwcwPojo pojo = new HwcwPojo();
                        pojo.setId(id);
                        pojo.setClassWork(class_work);
                        pojo.setDailyUpdateId(daily_update_id);
                        pojo.setHomeWork(home_work);
                        pojo.setTime(time);
                        pojo.setSubjectId(subject_id);

                        hwcwList.add(pojo);
                    }
                }

                setAdapter();
            }else{
                //error
                String message = jo.getString("message");
                Utils.showRedSnackbar(layoutForSnackbar,message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG,"HUS: parseResponse: "+e.getMessage());
            Utils.showParsingErrorAlert(HomeWorkCommonActivity.this);
        }
    }

    /*
    * Set adapter
    * */
    private void setAdapter() {
        HwcwAdapter adapter = new HwcwAdapter(hwcwList,mType);
        rv.setAdapter(adapter);
    }

    /*
    * Method to save JSON to Cache
    * */
    private void saveJsonToCache(String response) {
        File cacheDir = getCacheDir();
        File file = new File(cacheDir.getAbsolutePath(),mType+"data"+mId+".txt");
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
            fOut.write(response.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "HUS: saveJsonToCache: "+e.getMessage());
        }finally {
            if (fOut != null){
                try {
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "HUS: saveJsonToCache: "+e.getMessage());
                }
            }
        }
    }

    private String getJsonFromCache(){
        File cacheDir = getCacheDir();
        File file = new File(cacheDir.getAbsolutePath(),mType+"data"+mId+".txt");
        FileInputStream fIs = null;
        try {
            fIs = new FileInputStream(file);
            int read;
            StringBuilder sb = new StringBuilder();
            while((read = fIs.read()) != -1){
                sb.append((char)read);
            }
            fIs.close();
            Log.d(TAG, "HUSS: getJsonFromCache: try blog run");
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "HUS: getJsonFromCache: "+e.getMessage());
            return null;
        }finally {
            Log.d(TAG, "HUSS: getJsonFromCache: Finally blog run");
            if (fIs != null){
                try {
                    fIs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "HUS: getJsonFromCache: "+e.getMessage());
                }
            }
        }
    }
}