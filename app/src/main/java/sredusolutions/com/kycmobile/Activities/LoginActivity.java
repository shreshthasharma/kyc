package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

import static sredusolutions.com.kycmobile.Utilities.Utils.showRedSnackbar;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private RequestQueue mRequestQueue;
    ProgressDialog pd;

    EditText mobile;
    View layoutForSnackbar;
    private MySharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mobile = (EditText) findViewById(R.id.mobileNumber);
        layoutForSnackbar = findViewById(R.id.layout);

        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        //init progressdialog
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));
        pd.setCancelable(false);

        //init sp
        sp = MySharedPreferences.getInstance(this);

        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mMobile = mobile.getText().toString().trim();
                checkInternetConnection(mMobile);
            }
        });
    }

    private void checkInternetConnection(String mMobile) {

        if (Utils.isNetworkAvailable()) {
            if (mMobile.isEmpty()) {
                showRedSnackbar(layoutForSnackbar, getString(R.string.fill_in_all_fields));
                return;
            }
            if (mMobile.length() < 10) {
                showRedSnackbar(layoutForSnackbar, "Invalid mobile number");
                return;
            }

            doLoginInBackground(mMobile);
        } else {
            Utils.noInternetSnackBar(this, layoutForSnackbar);
        }
    }


    private void doLoginInBackground(final String mMobile) {
        pd.show();
        StringRequest sRequest = new StringRequest(Request.Method.POST, Endpoints.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pd.dismiss();
                Log.e(TAG,"HUS: "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String error = jsonObject.getString("error");
                    //success
                    if (error.equals("false")){

                        //parse data
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        String name = dataObj.getString("name");
                        String email = dataObj.getString("email");
                        String mobile = dataObj.getString("mobile");
                        String apiKey = dataObj.getString("apiKey");
                        String id = dataObj.getString("id");

                        //save to sp
                        sp.setKey(MySharedPreferences.KEY_USER_ID,id);
                        sp.setKey(MySharedPreferences.KEY_APIKEY,apiKey);
                        sp.setKey(MySharedPreferences.KEY_EMAIL,email);
                        sp.setKey(MySharedPreferences.KEY_NAME,name);
                        sp.setKey(MySharedPreferences.KEY_MOBILE,mobile);
                        sp.setKey(MySharedPreferences.KEY_STATE,"otp");

                        //send user to HomeActivity
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }else{
                        //error
                        String message = jsonObject.getString("message");
                        Utils.showRedSnackbar(layoutForSnackbar,message+"");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "HUS:doLoginInBackground:onResponse " + e.getMessage());
                    Utils.showParsingErrorAlert(LoginActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG,"HUS: doLoginInBackground: onErrorResponse "+error.getMessage());
                Utils.showParsingErrorAlert(LoginActivity.this);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", "abc");
                params.put("email","abc@gmail.com");
                params.put("mobile",mMobile);
                return params;
            }
        };

        mRequestQueue.add(sRequest);

    }


}
