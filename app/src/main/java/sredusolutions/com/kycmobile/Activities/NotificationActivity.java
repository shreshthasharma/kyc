package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sredusolutions.com.kycmobile.Adapter.NotificationAdapter;
import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.Pojo.NotificationModal;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

public class NotificationActivity extends AppCompatActivity {
    private static final String TAG = NotificationActivity.class.getSimpleName();

    private RequestQueue mRequestQueue;
    ProgressDialog pd;

    View layoutForSnackbar;

    public List<NotificationModal> notificationlist;
    private MySharedPreferences sp;

    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        layoutForSnackbar = findViewById(R.id.notification_layout);


        setSupportActionBar(mToolbar);
        assert getSupportActionBar() !=null;
        getSupportActionBar().setTitle("Notification");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        mRequestQueue = MyVolley.getInstance().getRequestQueue();
        //Initializing list
        notificationlist = new ArrayList<>();

        //init progressdialog
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));
        pd.setCancelable(false);

        sp = MySharedPreferences.getInstance(this);

        rv = (RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        checkInternetConnection();
    }

    private void checkInternetConnection() {
        if(Utils.isNetworkAvailable()){
            doCollingBackground();
        }else {
            Utils.noInternetSnackBar(this,layoutForSnackbar);
        }
    }

    private void doCollingBackground() {
        pd.show();
        StringRequest sRequest = new StringRequest(Request.Method.POST, Endpoints.NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                Log.e("Response",response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Utils.showParsingErrorAlert(NotificationActivity.this);

                Log.e(TAG,"HUS: doCollingBackground: onErrorResponse "+error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api", sp.getKey(MySharedPreferences.KEY_APIKEY));
                params.put("name", sp.getKey(MySharedPreferences.KEY_NAME));
                params.put("email", sp.getKey(MySharedPreferences.KEY_EMAIL));
                params.put("mobile", sp.getKey(MySharedPreferences.KEY_MOBILE));
                return params;
            }
        };
        mRequestQueue.add(sRequest);

    }

    private void parseResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String error = jsonObject.getString("error");
            if (!error.equals("true")){
                JSONArray ja = jsonObject.getJSONArray("data");

                for (int i = 0; i < ja.length() ; i++) {
                    JSONObject jo = ja.getJSONObject(i);
                    String id = jo.getString("id");
                    String addedDate = Utils.getDate(Long.parseLong(jo.getString("addedDate")));
                    String classNotes = jo.getString("classNotes");
                    String classroomId = jo.getString("classroomId");
                    String dateModified = jo.getString("dateModified");
                    String schoolId = jo.getString("schoolId");
                    String smsText = jo.getString("smsText");
                    String addedBy = jo.getString("addedBy");

                    NotificationModal pojo = new NotificationModal(id,
                            addedDate,
                            classNotes,
                            classroomId,
                            dateModified,
                            schoolId,
                            smsText,
                            addedBy);

                    notificationlist.add(pojo);
                }

                setupAdapter();
            }else{
                //error
                String message = jsonObject.getString("message");
                Utils.showRedSnackbar(layoutForSnackbar,message);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG,"HUS: parseResponse: "+e.getMessage());
            Utils.showParsingErrorAlert(NotificationActivity.this);
        }
    }

    private void setupAdapter() {
        NotificationAdapter adapter = new NotificationAdapter(this,notificationlist);
        rv.setAdapter(adapter);
    }


}
