package sredusolutions.com.kycmobile.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import sredusolutions.com.kycmobile.Constant.Endpoints;
import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;
import sredusolutions.com.kycmobile.Utilities.Utils;

import static sredusolutions.com.kycmobile.Utilities.Utils.showRedSnackbar;

public class OtpActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ProgressDialog pd;
    private MySharedPreferences sp;

    EditText otp;
    View layoutForSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        otp = (EditText) findViewById(R.id.otp_code);
        layoutForSnackbar = findViewById(R.id.otp_layout);

        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        //init progressdialog
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));
        pd.setCancelable(false);

        sp = MySharedPreferences.getInstance(this);

        Button verify = (Button) findViewById(R.id.otp_button);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mOtp = otp.getText().toString().trim();
                checkInternetConnection(mOtp);
            }
        });
    }

    private void checkInternetConnection(String mOtp) {
        if (Utils.isNetworkAvailable()) {
            if (mOtp.isEmpty()) {
                showRedSnackbar(layoutForSnackbar, getString(R.string.fill_in_all_fields));
                return;
            }
            doLoginInBackground(mOtp);
        } else {
            Utils.noInternetSnackBar(this, layoutForSnackbar);
        }
    }

    private void doLoginInBackground(final String mOtp) {
        pd.show();
        StringRequest sRequest = new StringRequest(Request.Method.POST, Endpoints.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String error = jsonObject.getString("error");

                    //success
                    if (error.equals("false")){

                        //parse data
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        String name = dataObj.getString("name");
                        String email = dataObj.getString("email");
                        String mobile = dataObj.getString("mobile");
                        String apiKey = dataObj.getString("apiKey");

                        //save to sp
                        sp.setKey(MySharedPreferences.KEY_APIKEY,apiKey);
                        sp.setKey(MySharedPreferences.KEY_EMAIL,email);
                        sp.setKey(MySharedPreferences.KEY_NAME,name);
                        sp.setKey(MySharedPreferences.KEY_MOBILE,mobile);
                        sp.setKey(MySharedPreferences.KEY_STATE,"login");

                        //send user to HomeActivity
                        Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }else{
                        //error
                        String message = jsonObject.getString("message");
                        Utils.showRedSnackbar(layoutForSnackbar,message+"");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "HUS:doLoginInBackground:onResponse " + e.getMessage());
                    Utils.showParsingErrorAlert(OtpActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e(TAG,"HUS: doLoginInBackground: onErrorResponse: "+error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("otp", mOtp);
                params.put("name", sp.getKey(MySharedPreferences.KEY_NAME));
                params.put("email", sp.getKey(MySharedPreferences.KEY_EMAIL));
                params.put("mobile", sp.getKey(MySharedPreferences.KEY_MOBILE));
                return params;
            }
        };

        mRequestQueue.add(sRequest);
    }
}
