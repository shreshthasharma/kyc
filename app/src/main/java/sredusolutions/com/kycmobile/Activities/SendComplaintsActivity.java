package sredusolutions.com.kycmobile.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import sredusolutions.com.kycmobile.Network.MyVolley;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Utilities.Utils;

import static sredusolutions.com.kycmobile.Utilities.Utils.showRedSnackbar;

public class SendComplaintsActivity extends AppCompatActivity {

    private static final String TAG = SendComplaintsActivity.class.getSimpleName();
    String COMPLAINTS_URL = "";

    private RequestQueue mRequestQueue;
    ProgressDialog pd;
    View layoutForSnackbar;

    Toolbar mToolbar;
    EditText name,email,complaints;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaints_activitry);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        name = (EditText)findViewById(R.id.etFullname);
        email = (EditText)findViewById(R.id.etEmail);
        complaints = (EditText)findViewById(R.id.etComplaints);
        layoutForSnackbar = findViewById(R.id.complaints_layout);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() !=null;
        getSupportActionBar().setTitle("Send Complaints");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        mRequestQueue = MyVolley.getInstance().getRequestQueue();

        //init progressdialog
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.processing));
        pd.setCancelable(false);

        final Button compSend = (Button)findViewById(R.id.btn_send);
        compSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mFullName = name.getText().toString().trim();
                String mEmail = email.getText().toString().trim();
                String mComplaints = complaints.getText().toString().trim();

                checkInternetConnection(mFullName,mEmail,mComplaints);

            }
        });


    }

    private void checkInternetConnection(String mFullName, String mEmail, String mComplaints) {

        if(Utils.isNetworkAvailable()){

            if(mFullName.isEmpty() || mEmail.isEmpty() || mComplaints.isEmpty()){
                showRedSnackbar(layoutForSnackbar,getString(R.string.fill_in_all_fields));
                return;
            }
            sendGmail(mFullName,mEmail,mComplaints);
        }else {
            Utils.noInternetSnackBar(this,layoutForSnackbar);
        }
    }

    private void sendGmail(String mFullName,String mEmail,String mComplaints) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto: info@kreiosinfo.com"));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Kyc data");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,"FullName :" + mFullName + "\n\nEmail :" + mEmail + "\n\nComplaints :" + mComplaints);
        startActivity(emailIntent);

        }



}
