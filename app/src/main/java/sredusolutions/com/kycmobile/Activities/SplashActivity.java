package sredusolutions.com.kycmobile.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Storage.MySharedPreferences;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3700;
    private Context context;
    private MySharedPreferences sp;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = MySharedPreferences.getInstance(this);

        if (sp.getKey(MySharedPreferences.KEY_STATE) != null
                && sp.getKey(MySharedPreferences.KEY_STATE).equals("login")){

            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }else if (sp.getKey(MySharedPreferences.KEY_STATE) != null
                && sp.getKey(MySharedPreferences.KEY_STATE).equals("otp")){

            Intent intent = new Intent(SplashActivity.this, OtpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;
        StartAnimations();
    }

    private void StartAnimations() {

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout l=(RelativeLayout) findViewById(R.id.activity_splash);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        LinearLayout iv = (LinearLayout) findViewById(R.id.image_layout);
        iv.clearAnimation();
        iv.startAnimation(anim);
        
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (sp.getKey(MySharedPreferences.KEY_STATE) == null){
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }
}
