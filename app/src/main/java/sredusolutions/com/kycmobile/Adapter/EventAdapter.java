package sredusolutions.com.kycmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Activities.EventDetailActivity;
import sredusolutions.com.kycmobile.Pojo.EventPojo;
import sredusolutions.com.kycmobile.R;

/**
 * Created by husain on 2/11/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder>  {
    private static final String TAG = EventAdapter.class.getSimpleName();
    private List<EventPojo> eventList;
    private Context mContext;

    public EventAdapter(List<EventPojo> eventList,Context context) {
        this.eventList = eventList;
        this.mContext = context;
    }

    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_view,parent,false);
        return new EventAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventAdapter.ViewHolder holder, int position) {
        EventPojo pojo = eventList.get(position);
        holder.tital.setText(pojo.getShortName());
        holder.detail.setText(pojo.getDetails());
        holder.startDate.setText(pojo.getStartDate());
        holder.endDate.setText(pojo.getEndDate());
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.tital) TextView tital;
        @Bind(R.id.detail) TextView detail;
        @Bind(R.id.startDate) TextView startDate;
        @Bind(R.id.endDate) TextView endDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();

            EventPojo pojo =  eventList.get(pos);

            Intent intent = new Intent(mContext,EventDetailActivity.class);

            intent.putExtra("endDate",pojo.getEndDate());
            intent.putExtra("starDate",pojo.getStartDate());
            intent.putExtra("detail",pojo.getDetails());
            intent.putExtra("shortName",pojo.getShortName());

            mContext.startActivity(intent);
        }
    }


}
