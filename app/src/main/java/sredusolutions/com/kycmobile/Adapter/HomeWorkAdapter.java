package sredusolutions.com.kycmobile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sredusolutions.com.kycmobile.Pojo.HomeworkModal;
import sredusolutions.com.kycmobile.R;

/**
 * Created by HackerKernel User on 2/11/2017.
 */

public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.ViewHolder>  {

    public Context context;
    public List<HomeworkModal>homeWork;

    public HomeWorkAdapter(Context context,List<HomeworkModal>homeWork){
        this.context = context;
        this.homeWork = homeWork;
    }

    @Override
    public HomeWorkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hwcw_list_row,parent,false);
        return new HomeWorkAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeWorkAdapter.ViewHolder holder, int position) {

        HomeworkModal home =  homeWork.get(position);

        holder.title.setText("");
        holder.description.setText("");

    }

    @Override
    public int getItemCount() {
        return homeWork.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title,description;

        public ViewHolder(View itemView) {
            super(itemView);
            //title = (TextView) itemView.findViewById(R.id.tv_title);
            //description= (TextView) itemView.findViewById(R.id.tv_description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int pos = getAdapterPosition();
        }
    }

}
