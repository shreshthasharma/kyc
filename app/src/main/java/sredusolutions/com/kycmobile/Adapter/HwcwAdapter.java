package sredusolutions.com.kycmobile.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Pojo.HwcwPojo;
import sredusolutions.com.kycmobile.R;
import sredusolutions.com.kycmobile.Utilities.Utils;

import static android.view.View.GONE;

/**
 * Created by husain on 2/21/2017.
 */

public class HwcwAdapter extends RecyclerView.Adapter<HwcwAdapter.HwcwViewHolder> {

    private List<HwcwPojo> list;
    private String type;

    public HwcwAdapter(List<HwcwPojo> list,String type) {
        this.list = list;
        this.type = type;
    }

    @Override
    public HwcwViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hwcw_list_row, parent, false);
        return new HwcwViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HwcwViewHolder holder, int position) {
        HwcwPojo pojo = list.get(position);

        if (type.equals("cw")){
           //classwork
            holder.title.setText(pojo.getClassWork());
        }else{
            //homework
            holder.title.setText(pojo.getHomeWork());
        }

        holder.description.setVisibility(GONE);
        if (!pojo.getTime().isEmpty()){
            String date = Utils.getDate(Long.parseLong(pojo.getTime()));
            holder.remark.setText(date);
        }else{
            holder.remark.setText(GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class HwcwViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.remark) TextView remark;

        public HwcwViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
