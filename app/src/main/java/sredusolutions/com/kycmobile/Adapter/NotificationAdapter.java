package sredusolutions.com.kycmobile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sredusolutions.com.kycmobile.Pojo.NotificationModal;
import sredusolutions.com.kycmobile.R;

/**
 * Created by HackerKernel User on 2/8/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    public Context context;

    public List<NotificationModal> notificationMsg;

    public NotificationAdapter(Context context,List<NotificationModal>notificationMsg) {
        this.notificationMsg=notificationMsg;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nitification_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationModal pojo = notificationMsg.get(position);
        holder.tv.setText(pojo.getAddedDate());
        holder.tv.append("\n\n"+pojo.getSmsText());
    }

    @Override
    public int getItemCount() {
        return notificationMsg.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv;

        public ViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.text);
        }
    }
}
