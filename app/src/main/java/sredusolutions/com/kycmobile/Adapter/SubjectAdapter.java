package sredusolutions.com.kycmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import sredusolutions.com.kycmobile.Activities.HomeWorkCommonActivity;
import sredusolutions.com.kycmobile.Pojo.SubjectPojo;
import sredusolutions.com.kycmobile.R;

/**
 * Created by husain on 2/21/2017.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.SubjectViewHolder> {
    private List<SubjectPojo> mList;
    private Context mContext;
    private String mType;

    public SubjectAdapter(Context mContext,List<SubjectPojo> list,String type) {
        this.mList = list;
        this.mContext = mContext;
        this.mType = type;
    }

    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list_row, parent, false);
        return new SubjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, int position) {
        SubjectPojo pojo = mList.get(position);
        holder.title.setText(pojo.getName());

        if(pojo.getName().equals("HINDI")){
            holder.image.setImageResource(R.drawable.hindi);

        }else if(pojo.getName().equals("MATHEMATICS")){
            holder.image.setImageResource(R.drawable.math);

        }else if (pojo.getName().equals("SCIENCE")){

            holder.image.setImageResource(R.drawable.science);

        }else if(pojo.getName().equals("PHYSICS")){
            holder.image.setImageResource(R.drawable.physics);

        }else if(pojo.getName().equals("CHEMISTRY")) {
            holder.image.setImageResource(R.drawable.chemistry);

        }else {
            holder.image.setImageResource(R.drawable.english);
        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SubjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.title) TextView title;
        @Bind(R.id.imageIcon) ImageView image;

        public SubjectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            SubjectPojo pojo = mList.get(pos);
            Intent intent = new Intent(mContext,HomeWorkCommonActivity.class);
            intent.putExtra("type",mType);
            intent.putExtra("id",pojo.getId());
            intent.putExtra("title",pojo.getName());
            mContext.startActivity(intent);
        }
    }
}
