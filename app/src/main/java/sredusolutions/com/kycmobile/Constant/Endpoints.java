package sredusolutions.com.kycmobile.Constant;

public class Endpoints {
    protected static String BASE_URL = "http://admin.sredusolutions.com/kycpush/otp/";
    public static String LOGIN = BASE_URL + "request_sms", //login
            VERIFY_OTP = BASE_URL + "verify_otp", //verify otp
            SUBJECTS = BASE_URL + "daily/subjects",//ALL SUBJECT
            HWCW = BASE_URL + "daily/hwcw",//HWCW
            NOTIFICATION = BASE_URL + "daily/recentall", //NOTIFICATION
            EVENTS = BASE_URL + "daily/events",
            HWCW_ALL = BASE_URL + "daily/hwcwall";
}
