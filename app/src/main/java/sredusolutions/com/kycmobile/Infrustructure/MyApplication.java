package sredusolutions.com.kycmobile.Infrustructure;

import android.app.Application;
import android.content.Context;

/**
 * Created by husain on 2/14/2017.
 */

public class MyApplication extends Application {

    public static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }



}
