package sredusolutions.com.kycmobile.Network;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import sredusolutions.com.kycmobile.Infrustructure.MyApplication;

/**
 * Created by husain on 2/14/2017.
 */

public class MyVolley {

    private static MyVolley mInstance = null;
    private RequestQueue mRequestQueue;

    protected MyVolley() {
        //request queue
        mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext());
    }

    public static MyVolley getInstance() {
        if (mInstance == null) {
            mInstance = new MyVolley();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

}
