package sredusolutions.com.kycmobile.Pojo;

/**
 * Created by husain on 2/21/2017.
 */

public class HwcwPojo {
    String id, classWork, dailyUpdateId, homeWork, time, subjectId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassWork() {
        return classWork;
    }

    public void setClassWork(String classWork) {
        this.classWork = classWork;
    }

    public String getDailyUpdateId() {
        return dailyUpdateId;
    }

    public void setDailyUpdateId(String dailyUpdateId) {
        this.dailyUpdateId = dailyUpdateId;
    }

    public String getHomeWork() {
        return homeWork;
    }

    public void setHomeWork(String homeWork) {
        this.homeWork = homeWork;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
