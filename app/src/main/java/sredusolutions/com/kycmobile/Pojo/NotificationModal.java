package sredusolutions.com.kycmobile.Pojo;

/**
 * Created by husain on 2/15/2017.
 */

public class NotificationModal  {
    private String id,addedDate,classNotes,classroomId,dateModified,schoolId,smsText,addedBy;

    public NotificationModal(String id,
                             String addedDate,
                             String classNotes,
                             String classroomId,
                             String dateModified,
                             String schoolId,
                             String smsText,
                             String addedBy){
        this.id = id;
        this.addedDate = addedDate;
        this.classNotes = classNotes;
        this.classroomId = classroomId;
        this.dateModified = dateModified;
        this.schoolId = schoolId;
        this.smsText = smsText;
        this.addedBy = addedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getClassNotes() {
        return classNotes;
    }

    public void setClassNotes(String classNotes) {
        this.classNotes = classNotes;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}
