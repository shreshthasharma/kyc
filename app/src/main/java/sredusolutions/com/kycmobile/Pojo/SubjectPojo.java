package sredusolutions.com.kycmobile.Pojo;

import android.graphics.drawable.Drawable;
import android.media.Image;

/**
 * Created by husain on 2/21/2017.
 */

public class SubjectPojo {




    private String id,name,shortName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
