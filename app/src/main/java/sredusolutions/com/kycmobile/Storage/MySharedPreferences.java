package sredusolutions.com.kycmobile.Storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Singleton class for sharedPreferences
 */
public class MySharedPreferences {
    //instance field
    private static SharedPreferences mSharedPreference;
    private static MySharedPreferences mInstance = null;
    private static Context mContext;


    //Shared Preference key
    private String KEY_PREFERENCE_NAME = "kyc";

    //private keyS
    private String KEY_DEFAULT = null;

    //user details keys
    public static final String KEY_MOBILE = "mobile",
            KEY_NAME = "name",
            KEY_EMAIL = "email",
            KEY_APIKEY = "apiKey",
            KEY_STATE = "state",
            KEY_USER_ID = "id";


    public MySharedPreferences() {
        mSharedPreference = mContext.getSharedPreferences(KEY_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public static MySharedPreferences getInstance(Context context) {
        mContext = context;
        if (mInstance == null) {
            mInstance = new MySharedPreferences();
        }
        return mInstance;
    }

    //Method to set boolean for (AppIntro)
    public void setBooleanKey(String keyname) {
        mSharedPreference.edit().putBoolean(keyname, true).apply();
    }

    public void setBooleanKey(String keyname, boolean state) {
        mSharedPreference.edit().putBoolean(keyname, state).apply();
    }

    /*
    * Method to get boolan key
    * true = means set
    * false = not set (show app intro)
    * */
    public boolean getBooleanKey(String keyname) {
        return mSharedPreference.getBoolean(keyname, false);
    }


    //Method to store user Mobile number
    public void setKey(String key,String value) {
        mSharedPreference.edit().putString(key, value).apply();
    }

    //Method to get User mobile number
    public String getKey(String key) {
        return mSharedPreference.getString(key, KEY_DEFAULT);
    }
}
